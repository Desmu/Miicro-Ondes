var Form = {
  disableWidths: function(){
    // Désactivation des largeurs indisponibles avec certains types de Mii.
    if (document.getElementById('type').value == "all_body") {
      document.getElementById('w96').selected = true;
      document.getElementById('w128').disabled = true;
      document.getElementById('w140').disabled = true;
    }
    else {
      document.getElementById('w128').disabled = false;
      document.getElementById('w140').disabled = false;
    }
  },
  formModify: function(type){
    // Modification des valeurs d'angles paramétrables dans le formulaire en fonction des axes de rotation sélectionnés.
    var value = 0;
    var keyword = "Rotate";
    var axis = "";
    if (type == "light") {
      keyword = "Direction";
      value = document.getElementById('lightType').value;
      axis = document.getElementById('lightAxisSettings').innerHTML;
    }
    else if (type == "character") {
      value = document.getElementById('characterType').value;
      axis = document.getElementById('characterAxisSettings').innerHTML;
    }
    else if (type == "camera") {
      value = document.getElementById('cameraType').value;
      axis = document.getElementById('cameraAxisSettings').innerHTML;
    }
    var tagX = "<label for='"+type+"X"+keyword+"'>X : </label><input type='number' min='0' max='359' id='"+type+"X"+keyword+"' name='"+type+"X"+keyword+"' value='0'> ";
    var tagY = "<label for='"+type+"Y"+keyword+"'>Y : </label><input type='number' min='0' max='359' id='"+type+"Y"+keyword+"' name='"+type+"Y"+keyword+"' value='0'> ";
    var tagZ = "<label for='"+type+"Z"+keyword+"'>Z : </label><input type='number' min='0' max='359' id='"+type+"Z"+keyword+"' name='"+type+"Z"+keyword+"' value='0'> ";
    switch(value){
      case "0":
        axis = tagX + tagY + tagZ;
        break;
      case "X": case "Xr":
        axis = tagY + tagZ;
        break;
      case "Y": case "Yr":
        axis = tagX + tagZ;
        break;
      case "Z": case "Zr":
        axis = tagX + tagY;
        break;
      case "XY": case "XYr":
        axis = tagZ;
        break;
      case "XZ": case "XZr":
        axis = tagY;
        break;
      case "YZ": case "YZr":
        axis = tagX;
        break;
      case "XYZ": case "XYZr":
        axis = '';
        break;
    }
    if (type == "light") document.getElementById('lightAxisSettings').innerHTML = axis;
    else if (type == "character") document.getElementById('characterAxisSettings').innerHTML = axis;
    else if (type == "camera") document.getElementById('cameraAxisSettings').innerHTML = axis;
  },
  availableGen: function(){
    // Activation du bouton de génération si une URL est donnée.
    if ((document.getElementById('url').value != "")&&(document.getElementById('url').value != null)) document.getElementById('gen').disabled = false;
    else document.getElementById('gen').disabled = true;
  },
  clickGen: function(){
    // Programme lancé après clic sur Générer.
    document.getElementById('final').innerHTML = '<a id="currURL" href="#"></a>';
    if ((document.getElementById('cameraType').value == "0")&&(document.getElementById('characterType').value == "0")&&(document.getElementById('lightType').value == "0")) {
      document.getElementById('final').innerHTML += '<progress id="progress" value="0" max="'+Number(document.getElementById('instanceCount').value)+'"></progress>';
    }
    else {
      document.getElementById('final').innerHTML += '<progress id="progress" value="0" max="359"></progress>';
    }
    MiiURL.initURL();
  }
};
