var MiiURL = {
  url: "",
  type: "face_only",
  width: "96",
  expression: "normal",
  clothesColor: "red",
  bgColor: "000000FF",
  cameraXRotate: "0",
  cameraYRotate: "0",
  cameraZRotate: "0",
  characterXRotate: "0",
  characterYRotate: "0",
  characterZRotate: "0",
  lightXDirection: "0",
  lightYDirection: "0",
  lightZDirection: "0",
  instanceCount: "1",
  initURL: function(){
    // Récupération des paramètres, construction de l'URL de base des images à partir des paramètres entrés.
    var tempurlavatar = document.getElementById('url').value.split("?")[0];
    if (tempurlavatar.indexOf("https://cdn-mii.accounts.nintendo.com/2.0.0/mii_images/") == 0) MiiURL.url = tempurlavatar+"?";
    MiiURL.type = document.getElementById('type').value;
    MiiURL.width = document.getElementById('width').value;
    MiiURL.expression = document.getElementById('expression').value;
    MiiURL.clothesColor = document.getElementById('clothesColor').value;
    var tabcolor = document.getElementById('bgColor').value.split('#');
    var alpha = parseInt(document.getElementById('bgAlpha').value).toString(16).toUpperCase();
    if (alpha.length == 1) alpha = "0"+alpha;
    MiiURL.bgColor = tabcolor[1].toUpperCase()+alpha;
    if (document.getElementById('instanceCount')) MiiURL.instanceCount = document.getElementById('instanceCount').value;
    var speed = document.getElementById('speed').value;
    var amount = document.getElementById('amount').value;
    var cameraType = document.getElementById('cameraType').value;
    var characterType = document.getElementById('characterType').value;
    var lightType = document.getElementById('lightType').value;
    var splitMode = document.getElementById('splitMode').checked;
    var urlCamera = MiiURL.setFixedURL(cameraType,"camera");
    var urlCharacter = MiiURL.setFixedURL(characterType,"character");
    var urlLight = MiiURL.setFixedURL(lightType,"light");
    MiiURL.url += "width="+MiiURL.width+"&type="+MiiURL.type+"&expression="+MiiURL.expression+"&clothesColor="+MiiURL.clothesColor+"&bgColor="+MiiURL.bgColor+urlCamera+urlCharacter+urlLight;
    if (splitMode) MiiURL.url += "&splitMode=back";
    Canvas.initCanvas(MiiURL,cameraType,characterType,lightType,speed,amount);
  },
  setFixedURL: function(animation,type){
    // Définit le ou les angles qui resteront fixes pendant le traitement.
    var fixedurl = "";
    var keyword = "Rotate";
    if (type == "light") keyword = "Direction";
    switch(animation) {
      case "0":
        fixedurl = "&"+type+"X"+keyword+"="+document.getElementById(type+'X'+keyword).value+"&"+type+"Y"+keyword+"="+document.getElementById(type+'Y'+keyword).value+"&"+type+"Z"+keyword+"="+document.getElementById(type+'Z'+keyword).value;
        break;
      case "X": case "Xr":
        fixedurl = "&"+type+"Y"+keyword+"="+document.getElementById(type+'Y'+keyword).value+"&"+type+"Z"+keyword+"="+document.getElementById(type+'Z'+keyword).value;
        break;
      case "Y": case "Yr":
        fixedurl = "&"+type+"X"+keyword+"="+document.getElementById(type+'X'+keyword).value+"&"+type+"Z"+keyword+"="+document.getElementById(type+'Z'+keyword).value;
        break;
      case "Z": case "Zr":
        fixedurl = "&"+type+"X"+keyword+"="+document.getElementById(type+'X'+keyword).value+"&"+type+"Y"+keyword+"="+document.getElementById(type+'Y'+keyword).value;
        break;
      case "XY": case "XYr":
        fixedurl = "&"+type+"Z"+keyword+"="+document.getElementById(type+'Z'+keyword).value;
        break;
      case "XZ": case "XZr":
        fixedurl = "&"+type+"Y"+keyword+"="+document.getElementById(type+'Y'+keyword).value;
        break;
      case "YZ": case "YZr":
        fixedurl = "&"+type+"X"+keyword+"="+document.getElementById(type+'X'+keyword).value;
        break;
      default:
        break;
    }
    return fixedurl;
  }
};
