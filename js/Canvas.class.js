var Canvas = {
  c: document.getElementById('images'),
  ctx: document.getElementById('images').getContext('2d'),
  tabPNG: new Array(),
  currPNG: new Image(),
  zip: new JSZip(),
  globalStep: 0,
  speed: 1,
  amount: 1,
  cameraType: "XYZ",
  sideCamera: "normal",
  stepCamera: 0,
  characterType: "XYZ",
  sideCharacter: "normal",
  stepCharacter: 0,
  lightType: "XYZ",
  sideLight: "normal",
  stepLight: 1,
  instanceStep: 1,
  boolInstance: false,
  initCanvas: function(url,cameraType,characterType,lightType,speed,amount){
    // Initialise le canvas et les propriétés étant liées au GIF.
    Canvas.c.height = url.width;
    Canvas.c.width = url.instanceCount * url.width;
    Canvas.ctx.fillStyle = "#"+url.bgColor;
    Canvas.speed = speed;
    Canvas.amount = amount;
    Canvas.cameraType = cameraType;
    Canvas.characterType = characterType;
    Canvas.lightType = lightType;
    Canvas.sideCamera = Canvas.sideAnimation(cameraType);
    Canvas.sideCharacter = Canvas.sideAnimation(characterType);
    Canvas.sideLight = Canvas.sideAnimation(lightType);
    if (Canvas.sideCamera == "normal") Canvas.stepCamera = 0;
    else if (Canvas.sideCamera == "reverse") Canvas.stepCamera = 359;
    if (Canvas.sideCharacter == "normal") Canvas.stepCharacter = 0;
    else if (Canvas.sideCharacter == "reverse") Canvas.stepCharacter = 359;
    if (Canvas.sideLight == "normal") Canvas.stepLight = 1;
    else if (Canvas.sideLight == "reverse") Canvas.stepLight = 360;
    Canvas.parsePNG(url);
  },
  sideAnimation: function(animation){
    // Définit le sens de rotation du paramètre concerné.
    switch(animation) {
      case "0":
        side = "none";
        break;
      case "X": case "Y": case "Z": case "XY": case "XZ": case "YZ": case "XYZ":
        side = "normal";
        break;
      case "Xr": case "Yr": case "Zr": case "XYr": case "XZr": case "YZr": case "XYZr":
        side = "reverse";
        break;
    }
    return side;
  },
  stepPNG: function(animation,type){
    // Construction de la partie variable de l'URL à récupérer.
    var keyword = "Rotate";
    var value = 0;
    var varurl = "";
    if (type == "light") {
      keyword = "Direction";
      value = Canvas.stepLight;
    }
    else if (type == "character") value = Canvas.stepCharacter;
    else if (type == "camera") value = Canvas.stepCamera;
    switch(animation) {
      case "0":
        break;
      case "X": case "Xr":
        varurl = "&"+type+"X"+keyword+"="+value;
        break;
      case "Y": case "Yr":
        varurl = "&"+type+"Y"+keyword+"="+value;
        break;
      case "Z": case "Zr":
        varurl = "&"+type+"Z"+keyword+"="+value;
        break;
      case "XY": case "XYr":
        varurl = "&"+type+"X"+keyword+"="+value+"&"+type+"Y"+keyword+"="+value;
        break;
      case "XZ": case "XZr":
        varurl = "&"+type+"X"+keyword+"="+value+"&"+type+"Z"+keyword+"="+value;
        break;
      case "YZ": case "YZr":
        varurl = "&"+type+"Y"+keyword+"="+value+"&"+type+"Z"+keyword+"="+value;
        break;
      case "XYZ": case "XYZr":
        varurl = "&"+type+"X"+keyword+"="+value+"&"+type+"Y"+keyword+"="+value+"&"+type+"Z"+keyword+"="+value;
        break;
      default:
        break;
    }
    return varurl;
  },
  parsePNG: function(url){
    // Initialisation de la boucle de récupération des images, en définissant l'angle et l'aplatissement à récupérer.
    var varurl = "";
    varurl += Canvas.stepPNG(Canvas.cameraType,"camera");
    varurl += Canvas.stepPNG(Canvas.characterType,"character");
    varurl += Canvas.stepPNG(Canvas.lightType,"light");
    Canvas.currPNG.src = url.url + varurl;
    Canvas.currPNG.setAttribute('crossOrigin','anonymous');
    Canvas.currPNG.onload = function(){
      // L'image récupérée est ajoutée au canvas, convertie en base64 et stockée localement.
      Canvas.ctx.clearRect(0,0,Canvas.c.width,Canvas.c.height);
      Canvas.ctx.drawImage(Canvas.currPNG,0,0);
      document.getElementById("currURL").href = Canvas.currPNG.src;
      document.getElementById("currURL").innerText = Canvas.currPNG.src;
      var b64 = Canvas.c.toDataURL();
      Canvas.zip.file("screens/"+Canvas.globalStep+".png",b64.replace(/^data:image\/(png|gif);base64,/, ""),{base64: true});
      Canvas.tabPNG.push(b64);
      Canvas.changeIMG(url);
    };
  },
  changeIMG: function(url){
    // Changement de l'URL de récupération de l'image.
    if (Canvas.sideCamera == "normal") Canvas.stepCamera = Canvas.stepCamera + (360 / Canvas.amount);
    else if (Canvas.sideCamera == "reverse") Canvas.stepCamera = Canvas.stepCamera - (360 / Canvas.amount);
    if (Canvas.sideCharacter == "normal") Canvas.stepCharacter = Canvas.stepCharacter + (360 / Canvas.amount);
    else if (Canvas.sideCharacter == "reverse") Canvas.stepCharacter = Canvas.stepCharacter - (360 / Canvas.amount);
    if (Canvas.sideLight == "normal") Canvas.stepLight = Canvas.stepLight + (360 / Canvas.amount);
    else if (Canvas.sideLight == "reverse") Canvas.stepLight = Canvas.stepLight - (360 / Canvas.amount);
    // Traitement en fin de récupération des images.
    if ((Canvas.sideCamera == "none")&&(Canvas.sideCharacter == "none")&&(Canvas.sideLight == "none")) {
      Canvas.createGIF();
    }
    else {
      Canvas.globalStep = Canvas.globalStep + (360 / Canvas.amount);
      document.getElementById('progress').value = Canvas.globalStep;
      if (Canvas.globalStep > 359) Canvas.createGIF();
      else setTimeout(function(){Canvas.parsePNG(url);},1000);
    }
  },
  createGIF: function(){
    // Création du GIF à partir des screens récupérés.
    Canvas.ctx.clearRect(0, 0,Canvas.c.width,Canvas.c.height);
    document.getElementById('final').innerHTML = "";
    document.getElementById('finalGIF').style.display = "inline";
    // Génération du ZIP contenant les screens.
    gifshot.createGIF({
      'images': Canvas.tabPNG,
      'frameDuration': Canvas.speed,
      'numFrames': Canvas.amount,
      'gifHeight': MiiURL.width,
      'gifWidth': MiiURL.width * MiiURL.instanceCount
    }, function(obj) {
      if(!obj.error) {
        var image = obj.image;
        var animatedImage = document.createElement('img');
        animatedImage.src = image;
        document.getElementById('finalGIF').innerHTML = "";
        document.getElementById("finalGIF").appendChild(animatedImage);
        Canvas.zip.file("mii.gif",image.replace(/^data:image\/(png|gif);base64,/, ""),{base64: true});
        Canvas.zip.generateAsync({type:"blob"}).then(function(blob){
          //document.getElementById('finalZIP').href = "data:application/zip;base64,"+base64;
          document.getElementById('finalZIP').href = URL.createObjectURL(blob);
          document.getElementById('finalZIP').style.display = "inline";
        });
      }
    });
  }
};
