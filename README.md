# Miicro-Ondes
Application utilisable à cette adresse : https://www.desmu.fr/miicroondes/ <br><br>

Application web non officielle permettant de récupérer 360 screens officiels pour faire un GIF de Mii pivotant autour d'un ou plusieurs axes.<br>
Le programme utilise les librairies Gifshot https://yahoo.github.io/gifshot/ et JSZip https://stuk.github.io/jszip/ .<br>
Il n'est pas encore possible de réaliser un GIF à fond transparent.
